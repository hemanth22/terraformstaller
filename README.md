# terraformstaller

Terraform installation playbook  

Command to pull the code:  

`ansible-pull -U https://gitlab.com/hemanth22/terraformstaller.git`

Command to pull the code, only if changes in the remote:  

`ansible-pull -o -U https://gitlab.com/hemanth22/terraformstaller.git`

Create below file.  

vi main.yaml
```
---
- hosts: servers
  remote_user: centos
  become: true
  become_method: sudo
  gather_facts: no
  tasks:
    - name: Install pip on the servers
      yum:
        name: python-pip
        state: latest
        update_cache: true
      become: true

    - name: Install pip3 on the servers
      yum:
        name: python3-pip
        state: latest        
      become: true

    - name: Ensure ansible is installed on servers
      pip:
        name: ansible

    - name: Executing ansible-pull on servers
      command: ansible-pull -U https://gitlab.com/hemanth22/terraformstaller.git

```

Create below inventory file.  

vi inventory
```
[servers]
18.236.236.207
[servers:vars]
ansible_python_interpreter=/usr/bin/python

```
```
.
├── inventory
├── main.yaml
```

Execute below command.  

`ansible-playbook main.yaml --private-key ~/.ssh/bitroid.pem -i inventory`
